module Servant.Misskey.Following (Api, ApiCreate, ApiDelete, Following (Following, userId)) where

import Data.Aeson (Options (sumEncoding), SumEncoding (UntaggedValue), ToJSON (toJSON), genericToJSON)
import GHC.Generics (Generic)
import Servant.API (JSON, Post, ReqBody, (:<|>), (:>))
import Servant.Misskey.Common (Authenticated, UserId, UserLite, defaultJSONOptions)

type Api = "following" :> (ApiCreate :<|> ApiDelete)
type ApiCreate = "create" :> ReqBody '[JSON] (Authenticated Following) :> Post '[JSON] UserLite
type ApiDelete = "delete" :> ReqBody '[JSON] (Authenticated Following) :> Post '[JSON] UserLite

newtype Following = Following {userId :: UserId}
  deriving stock (Generic, Show)

instance ToJSON Following where
  toJSON = genericToJSON defaultJSONOptions {sumEncoding = UntaggedValue}
