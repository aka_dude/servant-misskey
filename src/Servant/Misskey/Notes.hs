module Servant.Misskey.Notes where

import Data.Aeson (FromJSON, ToJSON (toJSON), genericToJSON)
import Data.Text (Text)
import GHC.Generics (Generic)
import Servant.API (JSON, Post, ReqBody, (:<|>), (:>))
import Servant.Misskey.Common (
  Authenticated,
  Note,
  NoteId,
  NoteVisibility,
  UserId,
  defaultJSONOptions,
 )

type Api = "notes" :> (ApiTimeline :<|> ApiGlobalTimeline :<|> ApiCreate)
type ApiTimeline = "timeline" :> ReqBody '[JSON] (Authenticated NotesTimelineRequest) :> Post '[JSON] [Note]
type ApiGlobalTimeline = "global-timeline" :> ReqBody '[JSON] (Authenticated NotesTimelineRequest) :> Post '[JSON] [Note]
type ApiCreate = "create" :> ReqBody '[JSON] (Authenticated NotesCreateRequest) :> Post '[JSON] NotesCreateResponse

data NotesTimelineRequest = NotesTimelineRequest
  { limit :: Maybe Integer
  , sinceId :: Maybe NoteId
  , untilId :: Maybe NoteId
  , sinceDate :: Maybe Integer
  , untilDate :: Maybe Integer
  }
  deriving stock (Generic, Show)

defaultNotesTimelineRequest :: NotesTimelineRequest
defaultNotesTimelineRequest =
  NotesTimelineRequest
    { limit = Nothing
    , sinceId = Nothing
    , untilId = Nothing
    , sinceDate = Nothing
    , untilDate = Nothing
    }

instance ToJSON NotesTimelineRequest where
  toJSON = genericToJSON defaultJSONOptions

data NotesCreateRequest = NotesCreateRequest
  { visibility :: Maybe NoteVisibility
  , visibleUserIds :: Maybe [UserId]
  , text :: Maybe Text
  , cw :: Maybe Text
  , viaMobile :: Maybe Bool
  , localOnly :: Maybe Bool
  , replyId :: Maybe NoteId
  , renoteId :: Maybe NoteId
  }
  deriving stock (Generic, Show)

instance ToJSON NotesCreateRequest where
  toJSON = genericToJSON defaultJSONOptions

defaultNotesCreateRequest :: NotesCreateRequest
defaultNotesCreateRequest =
  NotesCreateRequest
    { visibility = Nothing
    , visibleUserIds = Nothing
    , text = Nothing
    , cw = Nothing
    , viaMobile = Nothing
    , localOnly = Nothing
    , replyId = Nothing
    , renoteId = Nothing
    }

newtype NotesCreateResponse = NotesCreateResponse {createdNote :: Note}
  deriving stock (Generic, Show)
  deriving anyclass (FromJSON)
