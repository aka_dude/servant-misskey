module Servant.Misskey.Paginated where

import Data.Aeson (KeyValue ((.=)), ToJSON (toJSON))
import Data.Aeson qualified as Aeson (Value (Null, Object))
import Data.Aeson.KeyMap qualified as Aeson (fromList)
import Data.Maybe (catMaybes)

data Paginated bound a = Paginated
  { sinceId, untilId :: Maybe bound
  , limit :: Maybe Integer
  , inner :: a
  }
  deriving stock (Show)

defPaginated :: a -> Paginated bound a
defPaginated inner = Paginated {sinceId = Nothing, untilId = Nothing, limit = Just 100, inner}

instance (ToJSON bound, ToJSON a) => ToJSON (Paginated bound a) where
  toJSON Paginated {sinceId, untilId, limit, inner} =
    let o' = Aeson.fromList $ catMaybes [("sinceId" .=) <$> sinceId, ("untilId" .=) <$> untilId, ("limit" .=) <$> limit]
     in case toJSON inner of
          Aeson.Object o -> Aeson.Object $ o <> o'
          Aeson.Null -> Aeson.Object o'
          other -> error $ "Paginated.ToJSON.toJSON: `toJSON inner` returned " <> show other

fetchAllDesc :: (Monad m, Ord b) => (r -> b) -> (Paginated b a -> m [r]) -> Paginated b a -> m [r]
fetchAllDesc key request = go []
  where
    go acc paginated = do
      resp <- request paginated
      case resp of
        [] -> pure acc
        xs ->
          let untilId = minimum (key <$> xs)
              paginated' = paginated {sinceId = Nothing, untilId = Just untilId}
           in go (xs <> acc) paginated'
