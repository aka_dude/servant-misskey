module Servant.Misskey.Users (Api, ApiShow, UsersRequest (UsersById, UsersByUsername, userId, username, host), Follower (..), Followee (..)) where

import Data.Aeson (FromJSON (parseJSON), Options (sumEncoding), SumEncoding (UntaggedValue), ToJSON (toJSON), genericParseJSON, (.=))
import Data.Aeson qualified as Aeson (Value (Null, Object))
import Data.Text (Text)
import GHC.Generics (Generic)
import Servant.API (JSON, Post, ReqBody, (:<|>), (:>))
import Servant.Misskey.Common (FollowingId, UserDetailed, UserId, UserLite, defaultJSONOptions)
import Servant.Misskey.Paginated (Paginated)

type Api = "users" :> (ApiShow :<|> ApiFollowers :<|> ApiFollowing)
type ApiShow = "show" :> ReqBody '[JSON] UsersRequest :> Post '[JSON] UserDetailed
type ApiFollowers = "followers" :> ReqBody '[JSON] (Paginated FollowingId UsersRequest) :> Post '[JSON] [Follower]
type ApiFollowing = "following" :> ReqBody '[JSON] (Paginated FollowingId UsersRequest) :> Post '[JSON] [Followee]

data UsersRequest
  = UsersById {userId :: UserId}
  | UsersByUsername {username :: Text, host :: Maybe Text}
  deriving stock (Generic, Show)

instance ToJSON UsersRequest where
  toJSON UsersById {userId} = Aeson.Object ["userId" .= userId]
  toJSON UsersByUsername {username, host} = Aeson.Object ["username" .= username, "host" .= maybe Aeson.Null toJSON host]

data Follower = Follower
  { id :: FollowingId
  , createdAt :: Text
  , followeeId, followerId :: UserId
  , follower :: UserLite
  }
  deriving stock (Generic, Show)

instance FromJSON Follower where
  parseJSON = genericParseJSON defaultJSONOptions {sumEncoding = UntaggedValue}

data Followee = Followee
  { id :: FollowingId
  , createdAt :: Text
  , followeeId, followerId :: UserId
  , followee :: UserLite
  }
  deriving stock (Generic, Show)

instance FromJSON Followee where
  parseJSON = genericParseJSON defaultJSONOptions {sumEncoding = UntaggedValue}
