module Servant.Misskey.Common where

import Control.Applicative ((<|>))
import Data.Aeson (FromJSON (parseJSON), Options (constructorTagModifier, fieldLabelModifier, omitNothingFields), ToJSON (toJSON), genericParseJSON, genericToJSON, (.:))
import Data.Aeson qualified as Aeson (Object, Value (Array, Null, Object, String), defaultOptions, withObject)
import Data.Aeson.KeyMap qualified as Aeson.KeyMap (insert, singleton)
import Data.Char qualified as Char (toLower)
import Data.List (dropWhileEnd, stripPrefix)
import Data.Map.Strict (Map)
import Data.Maybe (fromJust)
import Data.String (IsString)
import Data.Text (Text)
import Data.Text qualified as Text (unpack)
import GHC.Generics (Generic)

defaultJSONOptions :: Options
defaultJSONOptions = Aeson.defaultOptions {omitNothingFields = True}

data Authenticated a = Authenticated {token :: Text, inner :: a}

instance (ToJSON a) => ToJSON (Authenticated a) where
  toJSON Authenticated {token, inner} = case toJSON inner of
    Aeson.Object o -> Aeson.Object $! Aeson.KeyMap.insert "i" (Aeson.String token) o
    Aeson.Array [] -> Aeson.Object $! Aeson.KeyMap.singleton "i" $! Aeson.String token
    Aeson.Null -> Aeson.Object $! Aeson.KeyMap.singleton "i" $! Aeson.String token
    other -> error $! "ToJSON (Authenticated a).toJSON: toJSON inner didn't return Aeson.Object: " <> show other

data NonExhaustive a = Known a | Unknown Aeson.Value
  deriving stock (Eq, Ord, Show)

instance (FromJSON a) => FromJSON (NonExhaustive a) where
  parseJSON j = Known <$> parseJSON j <|> pure (Unknown j)

newtype Id = Id Text
  deriving newtype (IsString, Eq, Ord, FromJSON, ToJSON)

instance Show Id where
  show (Id i) = "id:" <> Text.unpack i

newtype UriRequest = UriRequest {uri :: Text}
  deriving stock (Generic, Show)

instance ToJSON UriRequest

data Note = Note
  { id :: NoteId
  , createdAt :: Text
  , text :: Maybe Text
  , cw :: Maybe Text
  , user :: User
  , userId :: UserId
  , reply :: Maybe Note
  , replyId :: Maybe NoteId
  , renote :: Maybe Note
  , renoteId :: Maybe NoteId
  , visibility :: NoteVisibility
  , myReaction :: Maybe Text
  , reactions :: Map Text Integer
  , renoteCount :: Integer
  , repliesCount :: Integer
  -- , files :: Maybe [DriveFile]
  -- , filesIds :: Maybe [DriveFileId]
  -- , poll :: Maybe Poll
  }
  deriving stock (Eq, Ord, Generic, Show)

instance FromJSON Note

data NoteVisibility
  = VisibilityPublic
  | VisibilityHome
  | VisibilityFollowers
  | VisibilitySpecified
  deriving stock (Eq, Ord, Generic, Show)

instance FromJSON NoteVisibility where
  parseJSON = genericParseJSON defaultJSONOptions {constructorTagModifier = fmap Char.toLower . fromJust . stripPrefix "Visibility"}

instance ToJSON NoteVisibility where
  toJSON = genericToJSON defaultJSONOptions {constructorTagModifier = fmap Char.toLower . fromJust . stripPrefix "Visibility"}

newtype NoteId = NoteId Id
  deriving newtype (Eq, Ord, FromJSON, ToJSON)

instance Show NoteId where
  show (NoteId i) = "note:" <> show i

{-
data DriveFile = DriveFile
  { id :: DriveFileId
  , createdAt :: Text
  , name :: Text
  , mimeType :: Text
  , md5 :: Text
  , size :: Integer
  , isSensitive :: Bool
  , blurhash :: Maybe Text
  , properties :: DriveFileProperties
  , url :: Maybe Text
  , thumbnailUrl :: Maybe Text
  , comment :: Maybe Text
  , user :: Maybe UserLite
  , userId :: Maybe UserId
  }
  deriving stock (Eq, Ord, Generic, Show)

instance FromJSON DriveFile where
  parseJSON =
    genericParseJSON
      defaultJSONOptions
        { fieldLabelModifier = \case
            "mimeType" -> "type"
            other -> other
        }

data DriveFileProperties = DriveFileProperties
  { width, heigth, orientation :: Maybe Integer
  , avgColor :: Maybe Text
  }
  deriving stock (Eq, Ord, Generic, Show)

instance FromJSON DriveFileProperties

newtype DriveFileId = DriveFileId Id
  deriving newtype (Eq, Ord, FromJSON, ToJSON)

instance Show DriveFileId where
  show (DriveFileId i) = "driveFile:" <> show i
-}

type User = UserLite

type UserDetailed = UserLite

data UserLite = UserLite
  { id :: UserId
  , username :: Text
  , host :: Maybe Text
  , name :: Maybe Text
  , onlineStatus :: OnlineStatus
  , avatarUrl :: Maybe Text
  , avatarBlurhash :: Maybe Text
  , emojis :: Map Text Text -- Map EmojiName Url
  , instance_ :: Maybe Aeson.Object
  {-
    name: Instance['name'];
    softwareName: Instance['softwareName'];
    softwareVersion: Instance['softwareVersion'];
    iconUrl: Instance['iconUrl'];
    faviconUrl: Instance['faviconUrl'];
    themeColor: Instance['themeColor'];
  -}
  }
  deriving stock (Eq, Ord, Generic, Show)

userLiteJsonOptions :: Options
userLiteJsonOptions = defaultJSONOptions {fieldLabelModifier = dropWhileEnd (== '_')}

instance FromJSON UserLite where
  parseJSON = genericParseJSON userLiteJsonOptions

instance ToJSON UserLite where
  toJSON = genericToJSON userLiteJsonOptions

newtype UserId = UserId Id
  deriving newtype (Eq, Ord, FromJSON, ToJSON)

instance Show UserId where
  show (UserId i) = "user:" <> show i

data OnlineStatus = StatusOnline | StatusActive | StatusOffline | StatusUnknown
  deriving stock (Eq, Ord, Generic, Show)

onlineStatusJsonOptions :: Options
onlineStatusJsonOptions = defaultJSONOptions {constructorTagModifier = fmap Char.toLower . fromJust . stripPrefix "Status"}

instance FromJSON OnlineStatus where
  parseJSON = genericParseJSON onlineStatusJsonOptions

instance ToJSON OnlineStatus where
  toJSON = genericToJSON onlineStatusJsonOptions

data Notification = Notification
  { id :: NotificationId
  , createdAt :: Text
  , variant :: NonExhaustive NotificationVariant
  }
  deriving stock (Eq, Ord, Show)

instance FromJSON Notification where
  parseJSON = Aeson.withObject "Notification" \o ->
    Notification
      <$> o .: "id"
      <*> o .: "createdAt"
      <*> parseJSON (Aeson.Object o)

data NotificationVariant
  = NotificationReaction
      { reaction :: Text
      , user :: User
      , userId :: UserId
      , note :: Note
      }
  | NotificationNote
      { noteVariant :: NotificationNoteVariant
      , user :: User
      , userId :: UserId
      , note :: Note
      }
  | NotificationFollow
      { followVariant :: NotificationFollowVariant
      , user :: User
      , userId :: UserId
      }
  deriving stock (Eq, Ord, Show)

instance FromJSON NotificationVariant where
  parseJSON = Aeson.withObject "NotificationVariant" \o ->
    o .: "type" >>= \case
      "reaction" ->
        NotificationReaction
          <$> o .: "reaction"
          <*> o .: "user"
          <*> o .: "userId"
          <*> o .: "note"
      "reply" ->
        NotificationNote NotificationNoteReply
          <$> o .: "user"
          <*> o .: "userId"
          <*> o .: "note"
      "renote" ->
        NotificationNote NotificationNoteRenote
          <$> o .: "user"
          <*> o .: "userId"
          <*> o .: "note"
      "quote" ->
        NotificationNote NotificationNoteQuote
          <$> o .: "user"
          <*> o .: "userId"
          <*> o .: "note"
      "mention" ->
        NotificationNote NotificationNoteMention
          <$> o .: "user"
          <*> o .: "userId"
          <*> o .: "note"
      "pollVote" ->
        NotificationNote NotificationNotePollVote
          <$> o .: "user"
          <*> o .: "userId"
          <*> o .: "note"
      "follow" ->
        NotificationFollow NotificationFollowFollow
          <$> o .: "user"
          <*> o .: "userId"
      "followRequestAccepted" ->
        NotificationFollow NotificationFollowRequestAccepted
          <$> o .: "user"
          <*> o .: "userId"
      "receiveFollowRequest" ->
        NotificationFollow NotificationFollowReceiveRequest
          <$> o .: "user"
          <*> o .: "userId"
      other -> fail $! "Unknown type " <> other

data NotificationNoteVariant
  = NotificationNoteReply
  | NotificationNoteRenote
  | NotificationNoteQuote
  | NotificationNoteMention
  | NotificationNotePollVote
  deriving stock (Eq, Ord, Show)

data NotificationFollowVariant
  = NotificationFollowFollow
  | NotificationFollowRequestAccepted
  | NotificationFollowReceiveRequest
  deriving stock (Eq, Ord, Show)

newtype NotificationId = NotificationId Id
  deriving newtype (Eq, Ord, FromJSON, ToJSON)

instance Show NotificationId where
  show (NotificationId i) = "notification:" <> show i

type NotificationType = Text

newtype FollowingId = FollowingId Id
  deriving newtype (Eq, Ord, FromJSON, ToJSON)

instance Show FollowingId where
  show (FollowingId i) = "following:" <> show i
