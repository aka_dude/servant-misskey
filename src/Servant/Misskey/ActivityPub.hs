module Servant.Misskey.ActivityPub (Api, ApiGet, ApiShow, ApGetResponse, ApShowResponse (ApShowResponseNote, ApShowResponseUser)) where

import Data.Aeson (FromJSON (parseJSON), Options (constructorTagModifier, sumEncoding), SumEncoding (TaggedObject, contentsFieldName, tagFieldName), genericParseJSON)
import Data.Aeson qualified as Aeson (Object)
import Data.List (stripPrefix)
import Data.Maybe (fromJust)
import GHC.Generics (Generic)
import Servant.API (JSON, Post, ReqBody, (:<|>), (:>))
import Servant.Misskey.Common (Authenticated, Note, UriRequest, UserDetailed, defaultJSONOptions)

type Api = "ap" :> (ApiGet :<|> ApiShow)
type ApiGet = "get" :> ReqBody '[JSON] (Authenticated UriRequest) :> Post '[JSON] ApGetResponse
type ApiShow = "show" :> ReqBody '[JSON] (Authenticated UriRequest) :> Post '[JSON] ApShowResponse

type ApGetResponse = Aeson.Object

data ApShowResponse
  = ApShowResponseNote Note
  | ApShowResponseUser UserDetailed
  deriving stock (Generic, Show)

instance FromJSON ApShowResponse where
  parseJSON =
    genericParseJSON
      defaultJSONOptions
        { sumEncoding = TaggedObject {tagFieldName = "type", contentsFieldName = "object"}
        , constructorTagModifier = fromJust . stripPrefix "ApShowResponse"
        }
