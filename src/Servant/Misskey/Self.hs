module Servant.Misskey.Self (
  Api,
  ApiSelf,
  ApiNotifications,
  SelfNotificationsRequest (..),
  defaultSelfNotificationsRequest,
) where

import Data.Aeson (ToJSON (toJSON), genericToJSON)
import GHC.Generics (Generic)
import Servant.API (JSON, Post, ReqBody, (:<|>), (:>))
import Servant.Misskey.Common (
  Authenticated,
  Notification,
  NotificationId,
  NotificationType,
  User,
  defaultJSONOptions,
 )

type Api = "i" :> (ApiSelf :<|> ApiNotifications)
type ApiSelf = ReqBody '[JSON] (Authenticated ()) :> Post '[JSON] User
type ApiNotifications = "notifications" :> ReqBody '[JSON] (Authenticated SelfNotificationsRequest) :> Post '[JSON] [Notification]

data SelfNotificationsRequest = SelfNotificationsRequest
  { limit :: Maybe Integer
  , sinceId :: Maybe NotificationId
  , untilId :: Maybe NotificationId
  , following :: Maybe Bool
  , unreadOnly :: Maybe Bool
  , markAsRead :: Maybe Bool
  , includeTypes :: Maybe [NotificationType]
  , excludeTypes :: Maybe [NotificationType]
  }
  deriving stock (Generic)

defaultSelfNotificationsRequest :: SelfNotificationsRequest
defaultSelfNotificationsRequest =
  SelfNotificationsRequest
    { limit = Nothing
    , sinceId = Nothing
    , untilId = Nothing
    , following = Nothing
    , unreadOnly = Nothing
    , markAsRead = Nothing
    , includeTypes = Nothing
    , excludeTypes = Nothing
    }

instance ToJSON SelfNotificationsRequest where
  toJSON = genericToJSON defaultJSONOptions
