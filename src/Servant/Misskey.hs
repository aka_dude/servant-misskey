module Servant.Misskey (Api) where

import Servant.API ((:<|>), (:>))
import Servant.Misskey.ActivityPub qualified as ActivityPub
import Servant.Misskey.Following qualified as Following
import Servant.Misskey.Notes qualified as Notes
import Servant.Misskey.Self qualified as Self
import Servant.Misskey.Users qualified as Users

type Api = "api" :> (ActivityPub.Api :<|> Self.Api :<|> Notes.Api :<|> Users.Api :<|> Following.Api)
