{-# OPTIONS_GHC -w #-}

module Main where

import Control.Monad.IO.Class (liftIO)
import Data.Aeson qualified as Aeson (Object)
import Data.ByteString.Lazy qualified as B
import Data.Foldable (traverse_)
import Data.Maybe (catMaybes)
import Data.Proxy (Proxy (Proxy))
import Data.Text (Text)
import Data.Text qualified as T (isInfixOf, pack)
import Data.Text.IO qualified as T (putStrLn)
import Network.HTTP.Client.TLS (newTlsManager)
import Servant.API (type (:<|>) ((:<|>)))
import Servant.Client
import Servant.Misskey qualified as Misskey (Api)
import Servant.Misskey.ActivityPub qualified as Misskey (ApShowResponse)
import Servant.Misskey.Common (
  Authenticated (Authenticated),
  Id (..),
  Note,
  NoteId (NoteId),
  Notification,
  UriRequest,
  UserLite (UserLite),
 )
import Servant.Misskey.Common qualified as Misskey (Note (..), UserLite (..))
import Servant.Misskey.Notes (defaultNotesCreateRequest, renoteId, replyId, text)
import Servant.Misskey.Notes qualified as Misskey (NotesCreateRequest, NotesCreateResponse, NotesTimelineRequest, defaultNotesTimelineRequest)
import Servant.Misskey.Notes qualified as Notes (NotesTimelineRequest (limit))
import Servant.Misskey.Paginated (defPaginated, fetchAllDesc, limit)
import Servant.Misskey.Self qualified as Misskey (SelfNotificationsRequest(..), defaultSelfNotificationsRequest)
import Servant.Misskey.Self qualified as Self (SelfNotificationsRequest (limit))
import Servant.Misskey.Users (Followee (followee, id), Follower (follower, id), UsersRequest (UsersById, userId))
import Servant.Misskey.Users qualified as Misskey (UsersRequest)
import System.Environment (getEnv)

apGet ::
  Authenticated UriRequest ->
  ClientM Aeson.Object
apShow :: Authenticated UriRequest -> ClientM Misskey.ApShowResponse
self :: Authenticated () -> ClientM UserLite
notifications :: Authenticated Misskey.SelfNotificationsRequest -> ClientM [Notification]
timeline, globalTimeline :: Authenticated Misskey.NotesTimelineRequest -> ClientM [Note]
noteCreate :: Authenticated Misskey.NotesCreateRequest -> ClientM Misskey.NotesCreateResponse
usersShow :: Misskey.UsersRequest -> ClientM UserLite
(apGet :<|> apShow)
  :<|> (self :<|> notifications)
  :<|> (timeline :<|> globalTimeline :<|> noteCreate)
  :<|> (usersShow :<|> userFollowers :<|> userFollowing)
  :<|> (followingCreate :<|> followingDelete) = client (Proxy @Misskey.Api)

queries :: Text -> ClientM ()
queries token = do
  -- me <- self (a ())
  -- liftIO $ print me

  traverse_ (liftIO . print) =<< notifications (a Misskey.defaultSelfNotificationsRequest {Misskey.markAsRead = Just False, Misskey.unreadOnly = Just True})

  -- followers <- fetchAllDesc (.id) userFollowers ((defPaginated UsersById {userId = me.id}) {limit = Just 100})
  -- traverse_ (liftIO . T.putStrLn . fmtUser . (.follower)) followers
  -- liftIO $ putStrLn $ "length followers = " <> show (length followers)

  -- following <- fetchAllDesc (.id) userFollowing (defPaginated UsersById {userId = me.id})
  -- traverse_ (liftIO . T.putStrLn . fmtUser . (.followee)) following
  -- liftIO $ putStrLn $ "length following = " <> show (length following)
  where
    a :: a -> Authenticated a
    a = Authenticated token

fmtUser :: UserLite -> Text
fmtUser UserLite {username, host, name} = mconcat $ catMaybes [Just $ "@" <> username, mappend "@" <$> host, mappend " " <$> name]

printWorthy :: Note -> Bool
printWorthy Misskey.Note {text = Just text} = any @[] (`T.isInfixOf` text) ["#rusmeme", "#мемцы"]
printWorthy _ = False

fmtNote :: Note -> Text
fmtNote Misskey.Note {user, text} =
  mconcat $
    catMaybes
      [ user.name
      , Just " "
      , Just user.username
      , Just "@"
      , user.host
      , Just ": "
      , text
      , Just "\n------------------"
      ]

main :: IO ()
main = do
  token <- T.pack <$> getEnv "MISSKEY_TOKEN"
  manager <- newTlsManager
  runClientM (queries token) (mkClientEnv manager (BaseUrl Https "shitpost.poridge.club" 443 "")) >>=
    \case
      Right () -> pure ()
      -- Left (DecodeFailure _ resp) -> B.putStr $ responseBody resp <> "\n"
      Left err -> print err
